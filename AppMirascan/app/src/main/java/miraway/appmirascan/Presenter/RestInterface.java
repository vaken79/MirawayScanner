package miraway.appmirascan.Presenter;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by Thanh_Dev on 18/01/2018.
 */

public interface RestInterface {
   // String url = "https://cogilo.com/api/login";
    String url = "http://mirascan.vn:31002/api/auth/login";
    //"https://www.cogilo.com/api/";
   // http://cogilo.com/api/login HTTP/1.1
    @FormUrlEncoded
    @POST("/login")
    void Login(@Field("email") String email,
               @Field("pass") String pass, Callback<LoginModel> cb);


    @FormUrlEncoded
    @POST("/signup")
    void SingUp(@Field("name") String name, @Field("email") String email,
                @Field("pass") String pass, Callback<LoginModel> pm);
}
