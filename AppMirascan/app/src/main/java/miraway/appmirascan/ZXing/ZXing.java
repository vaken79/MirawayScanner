package miraway.appmirascan.ZXing;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import miraway.appmirascan.R;

public class ZXing extends AppCompatActivity implements ZXingScannerView.ResultHandler{
    private ZXingScannerView mScannerView;
    Button btn;
    public static final String txtQR = "txtQR";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_zxing);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();


    }



    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.startCamera();
    }



    @Override
    public void handleResult(Result result) {
        final String txtQrcode = result.getText().toString();

        Log.v("HandleResult",txtQrcode);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Scan result");
        builder.setMessage(result.getText());
        builder.setCancelable(false);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Toast.makeText(ZXing.this,"OK roi",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ZXing.this,webV.class);
                intent.putExtra(txtQR,txtQrcode);
                startActivity(intent);
                finish();

            }
        });

//        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.cancel();
//            }
//        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
//        mScannerView.resumeCameraPreview(this);
        //return txtQrcode;
    }


}
