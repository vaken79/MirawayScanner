package miraway.appmirascan.ZXing;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

import miraway.appmirascan.R;

public class webV extends AppCompatActivity {
    EditText editText;
    WebView webView;
    Button btnGo;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_v);


//        webView.setWebViewClient(new MyBrowser());
//        Intent intent = getIntent();
//        //editText.setText(intent.getStringExtra(ZXing.txtQR));
//
//        final String  urL = intent.getStringExtra(ZXing.txtQR);
//        progressDialog = ProgressDialog.show(webV.this,null,"Loading ...");
//        webView.getSettings().setLoadsImagesAutomatically(true);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        webView.loadUrl(urL);
        initWebView();

    }

//    private class MyBrowser extends WebViewClient {
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            if (progressDialog.isShowing()){
//                progressDialog.dismiss();
//            }
//           view.loadUrl(url);
//            return true;
//        }




    @SuppressLint("JavascriptInterface")
    private void initWebView(){
        webView  = (WebView) findViewById(R.id.wv);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        webView.setWebViewClient(new WebViewClient(){

                                     @Override
                                     public void onPageStarted(WebView view, String url, Bitmap favicon) {
                                         super.onPageStarted(view, url, favicon);
                                     }

                                     @Override
                                     public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                         view.loadUrl(url);
                                         return true;
                                     }

                                     @Override
                                     public void onPageFinished(WebView view, String url) {
                                         setJavascript();
                                     }
                                 }

        );
        Intent intent = getIntent();
        final String  urL = intent.getStringExtra(ZXing.txtQR);
        webView.loadUrl(urL);
    }


    class MyJavaScriptInterface {
        @JavascriptInterface
        public void scrollWidth(String jsResult) {
            Log.d("DAT", "Width webview = " + jsResult);
        }
    }




    private void setJavascript(){
        webView.loadUrl("javascript:( " +
                "function () {" +
                " var result = document.documentElement.scrollWidth; window.HTMLOUT.scrollWidth(result); " +
                "} ) ()"
        );
    }











}
