package miraway.appmirascan.UI;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import miraway.appmirascan.Presenter.LoginModel;
import miraway.appmirascan.R;
import miraway.appmirascan.ZXing.ZXing;

public class SigupActivity extends AppCompatActivity {
    EditText edEmail,edPass;
    Button btnSigupp;
    private ProgressDialog mProgressDialog;
    private final String ServerUrl = "http://mirascan.vn:31002/api/user/create";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sigup);
        edEmail = (EditText) findViewById(R.id.mEmail);
        edPass = (EditText) findViewById(R.id.mPassword);
        btnSigupp = (Button) findViewById(R.id.btnSig);
        Prog();
        btnSigupp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gs = new Gson();
                String username = edEmail.getText().toString();
                String password = edPass.getText().toString();
                LoginModel lg = new LoginModel(username, password);
                String jsonlg = gs.toJson(lg);
                new sigup().execute(ServerUrl,jsonlg);
            }
        });
    }


    private void Prog(){
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Đang đăng ký...");
        mProgressDialog.setProgressStyle(ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    class sigup extends AsyncTask<String,Void,Boolean> {
        private ProgressDialog dialog = new ProgressDialog(SigupActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            //viết chức năng đăng nhập
            boolean bn = false;
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(600000);
                connection.setReadTimeout(60000);
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestMethod("GET"); // phương thức truy nhập
                connection.setRequestProperty("Content-Type","application/json"); // kiểu truy nhập
                String jsonLogin = params[1];
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(jsonLogin.getBytes());
                outputStream.close();
                if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                    bn = true;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bn;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mProgressDialog.dismiss();
            if(aBoolean){
                Toast.makeText(SigupActivity.this,"Đăng ký thành công",Toast.LENGTH_LONG).show();
                Intent i = new Intent(SigupActivity.this,LoginActivity.class);
                startActivity(i);
                finish();
            }
            else {
                Toast.makeText(SigupActivity.this,"Đăng ký không thành công",Toast.LENGTH_LONG).show();
            }
        }
    }
}
