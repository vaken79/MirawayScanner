package miraway.appmirascan.UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import miraway.appmirascan.Presenter.LoginModel;
import miraway.appmirascan.R;
import miraway.appmirascan.ZXing.ZXing;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    EditText mEmailView,mPasswordView;
    TextView btnSigup;
    Button btnLogin;
    Boolean savelogin;
    private CallbackManager mCallbackManager;
    private ProgressDialog mProgressDialog;
    private final String ServerUrl = "http://mirascan.vn:31002/api/auth/login";
    private CheckBox rem_userpass;
    private static final String KEY_REMEMBER = "remember";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String MyPREFERENCES = "MyPREFERENCES";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEmailView = (EditText) findViewById(R.id.mEmailView);
        mPasswordView = (EditText) findViewById(R.id.mPasswordView);
        btnSigup = (TextView) findViewById(R.id.btnSigup);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);



        btnSigup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent iSg = new Intent(LoginActivity.this,SigupActivity.class);
                startActivity(iSg);

            }
        });
        btnSigup.setPaintFlags(btnSigup.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        Prog();
        findViewById(R.id.loginfb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallbackManager == null){
                    mCallbackManager = CallbackManager.Factory.create();
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email","public_profile"));
                    LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            LoginFB(loginResult);
                            Intent i = new Intent(LoginActivity.this,ZXing.class);
                            startActivity(i);
                            finish();
//                            Intent i = new Intent(LoginActivity.this,Menu.class);
//                            startActivity(i);
                            Toast.makeText(LoginActivity.this, "Login Facebook success.", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onCancel() {

                        }

                        @Override
                        public void onError(FacebookException error) {
                            Toast.makeText(LoginActivity.this, "Login Facebook Error.", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode,resultCode,data);
    }


    private void LoginFB(LoginResult loginResult){
        GraphRequest graphRequest = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {
                            final String email = object.getString("email");
                            final String socialId = object.getString("id");
                            // Toast.makeText(LoginActivity.this, "" + object.optString("name"), Toast.LENGTH_LONG).show();
                            mProgressDialog.show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
        );
        Bundle parameters = new Bundle();
        parameters.putString("fields","email,id");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }
    private void Prog(){
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Đang đăng nhập...");
        mProgressDialog.setProgressStyle(ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }
    @Override
    public void onClick(View view) {
        Gson gs = new Gson();

        sharedPreferences = getSharedPreferences("RememberUser", Context.MODE_PRIVATE);

        String username = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        editor = sharedPreferences.edit();

        editor.putString("URname",username);
        editor.putString("URpass",password);
        editor.commit();

        LoginModel lg = new LoginModel(mEmailView.getText().toString(), mPasswordView.getText().toString());
        String jsonlg = gs.toJson(lg);
        new login().execute(ServerUrl,jsonlg);
    }
    class login extends AsyncTask<String,Void,Boolean> {
        private ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            //viết chức năng đăng nhập
            boolean bn = false;
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(600000);
                connection.setReadTimeout(60000);
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestMethod("POST"); // phương thức truy nhập
                connection.setRequestProperty("Content-Type","application/json"); // kiểu truy nhập
                String jsonLogin = params[1];
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(jsonLogin.getBytes());
                outputStream.close();
                if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                    bn = true;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bn;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mProgressDialog.dismiss();
            if(aBoolean){
                Toast.makeText(LoginActivity.this,"Đăng nhập thành công",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(LoginActivity.this,Menu.class);
                 startActivity(i);
                 finish();
            }
            else {
                Toast.makeText(LoginActivity.this,"Sai mật khẩu hoặc tài khoản.",Toast.LENGTH_LONG).show();
            }
        }
    }



}
