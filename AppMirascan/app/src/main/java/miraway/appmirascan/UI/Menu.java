package miraway.appmirascan.UI;

import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import miraway.appmirascan.R;
import miraway.appmirascan.User.UserAcounnt;
import miraway.appmirascan.ZXing.QrCodeActivity;
import miraway.appmirascan.ZXing.ZXing;


public class Menu extends AppCompatActivity {
    ImageView btnTruyx,avatar,ScanQrr,Truyxuatt,q1,q2;
    LinearLayout linearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout1);
        avatar = (ImageView) findViewById(R.id.avatarr);
        ScanQrr = (ImageView) findViewById(R.id.ScanQr);
        Truyxuatt = (ImageView) findViewById(R.id.TruyXuat);
        q1= (ImageView) findViewById(R.id.q1);
        q2= (ImageView) findViewById(R.id.q2);

       // Truyxuatt.setOnClickListener(Menu.this);
        BottomNavigationView bottomNavigationView  = (BottomNavigationView) findViewById(R.id.menu_nav);
        //ScanQrr.setOnClickListener(this);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.Scannerr:
//
                        Intent i = new Intent(Menu.this,ZXing.class);
                        startActivity(i);

//                        q1.setVisibility(View.VISIBLE);
//                        q2.setVisibility(View.GONE);
//                        ScanQrr.setVisibility(View.GONE);
                        break;
                    case R.id.neotool:
//                        linearLayout.setVisibility(View.VISIBLE);
                        Toast.makeText(Menu.this,"đang phát triển",Toast.LENGTH_SHORT).show();
//                        q1.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.VISIBLE);
//                        ScanQrr.setVisibility(View.GONE);
                        break;
                    case R.id.history:
//                        linearLayout.setVisibility(View.VISIBLE);
                        Toast.makeText(Menu.this,"lịch sử đang phát triển",Toast.LENGTH_SHORT).show();
//                        q1.setVisibility(View.GONE);
//                        q1.setVisibility(View.GONE);
//                        ScanQrr.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });


        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent iAcounnt = new Intent(Menu.this, UserAcounnt.class);
                startActivity(iAcounnt);
            }
        });



    }


//    @Override
//    public void onClick(View v) {
//
//        switch (v.getId()){
//            case R.id.ScanQr:
//                Intent i = new Intent(Menu.this,ZXing.class);
//                startActivity(i);
//                break;
//            case R.id.TruyXuat:
//                Intent intent = new Intent(Menu.this, QrCodeActivity.class);
//                startActivity(intent);
//                break;
//        }
//    }
}
